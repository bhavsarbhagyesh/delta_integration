package Util;

import java.util.List;

/**
 * @author bhavsarb
 */
public class SosvVO {
    private long sosvPercent;
    public List<Object> shareSosvBar;

    public long getSosvPercent() {
        return sosvPercent;
    }

    public void setSosvPercent(long sosvPercent) {
        this.sosvPercent = sosvPercent;
    }

    public List<Object> getShareSosvBar() {
        return shareSosvBar;
    }

    public void setShareSosvBar(List<Object> shareSosvBar) {
        this.shareSosvBar = shareSosvBar;
    }

    @Override
    public String toString() {
        return "SosvVO [sosvPercent=" + sosvPercent + ", shareSosvBar=" + shareSosvBar + "]";
    }

}
