package scenarios;

import Helper.CreateSessionOfUser;
import Util.SosvVO;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;

import java.util.HashMap;

/**
 * @author bhavsarb
 */

@RunWith(JUnit4.class)
public class Dashboard {

    private StringBuilder SessionToken=null;
    CreateSessionOfUser CreateSessionOfUser =null;
    RestTemplate restTemplate =null;
    HashMap<String,String> auth = null;

    /*Create Session for user and create hashmap of token for api calss*/
    @Before
    public void setUp(){

        CreateSessionOfUser = new CreateSessionOfUser();
        SessionToken = CreateSessionOfUser.getBearearToken();
        restTemplate = new RestTemplate();
        auth = new HashMap<String, String>();
        auth.put("Authorization",SessionToken.toString());

    }


    /*Test all the dashboard api of MEDMEME delta*/
    @Test
    public void testSOSVCanad1YearMeetings() throws UnirestException {


        HttpResponse<JsonNode> jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/sosv/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=1"))
                .headers(auth)
                .asJson();


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosvPercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("shareSosvBar"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugname"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sov"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosv"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sharePercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totaldistinctmentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totalmentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusDrug"));


    }


    @Test
    public void testReachByTiersCanada1YearMeetings(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/reachbytiers/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=1")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("reachByTiers"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totalAoiAbstracts"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totalFilteredAbstracts"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("reachByTiers"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("tier"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("tierAbstractCount"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosv"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosv"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugname"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusdrug"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sov"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosv"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sharePercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totaldistinctmentions"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totalmentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusDrug"));





    }

    @Test
    public void testMentionsCanada1YearMeetings(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/mentions/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=1"))
                    .headers(auth)
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());


        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentionDate"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("month"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("year"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("articlescore"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("cummentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusdrug"));


    }

    @Test
    public void testImpactMultipleCanada1YearMeetings(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/impact_multiple/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=1")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusDrug"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("impactMultiple"));


    }

    ///api/dashboard/sosi/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=1


    @Test
    public void testSOSICanada1YearMeetings(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/sosi/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=1")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosiPercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("shareSosiBar"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sharePercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("impactPercentage"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusDrug"));
    }



    @Test
    public void testImpactOverTimeCanada1YearMeetings(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/impact_over_time/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=1")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());


        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentionDate"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("month"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("year"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("articlescore"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("cummentions"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("impactvalue"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("cumimpactvalue"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusdrug"));
    }



    //Journals

    @Test
    public void testSOSVCanad1YearJournals() throws UnirestException {


        HttpResponse<JsonNode> jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/sosv/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=2"))
                .headers(auth)
                .asJson();


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosvPercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("shareSosvBar"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugname"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sov"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosv"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sharePercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totaldistinctmentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totalmentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusDrug"));


    }


    @Test
    public void testReachByTiersCanada1YearJournals(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/reachbytiers/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=2")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("reachByTiers"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totalAoiAbstracts"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totalFilteredAbstracts"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("reachByTiers"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("tier"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("tierAbstractCount"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosv"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosv"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugname"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusdrug"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sov"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosv"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sharePercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totaldistinctmentions"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("totalmentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusDrug"));





    }

    @Test
    public void testMentionsCanada1YearJournals(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/mentions/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=2")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());


        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentionDate"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("month"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("year"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("articlescore"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("cummentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusdrug"));


    }

    @Test
    public void testImpactMultipleCanada1YearJournals(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/impact_multiple/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=2")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusDrug"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("impactMultiple"));


    }

    ///api/dashboard/sosi/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=1


    @Test
    public void testSOSICanada1YearJournals(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/sosi/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=2")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sosiPercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("shareSosiBar"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("sharePercent"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("impactPercentage"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusDrug"));
    }



    @Test
    public void testImpactOverTimeCanada1YearJournals(){
        HttpResponse<JsonNode> jsonResponse =null;
        try {
            jsonResponse = Unirest.get(CreateSessionOfUser.createURLWithPort("/api/dashboard/impact_over_time/10?regionid=12&fromDate=06/01/2017&toDate=05/01/2018&sourceTypeId=2")).headers(auth).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


        assertEquals(200,jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody());


        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugid"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("drugName"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentionDate"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("month"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("year"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("mentions"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("articlescore"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("cummentions"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("impactvalue"));

        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("cumimpactvalue"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("ordinal"));
        assertThat(jsonResponse.getBody().toString(), CoreMatchers.containsString("focusdrug"));
    }






}
